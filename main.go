package main

import (
	"fmt"

	"github.com/pterm/pterm"
	"pnda.cloud/cli/internal/console"

	_ "embed"
)

// type about struct {
// 	versionTag  string
// 	sha1Version string
// 	buildTime   string
// }

// Filled on build
var (
	//go:embed version.txt
	versionTag  string // Version tag of the program
	sha1Version string // sha1 revision used to build the program, INJECTED ON BUILD
	buildTime   string // when the executable was built, INJECTED ON BUILD
)

func main() {
	console.ReadCliArguments()

	fmt.Println("Version", versionTag)
	fmt.Println("SHA1", sha1Version)
	fmt.Println("Build time", buildTime)

	verifySystemRequirements()
}

func verifySystemRequirements() {
	systemRequirementsCount := 2

	pterm.DefaultSection.Println("Verify System Requirements")

	p, _ := pterm.DefaultProgressbar.WithTotal(systemRequirementsCount).WithTitle("Verify system requirements").Start()

	if console.CommandExists("docker") == true {
		pterm.Success.Println("Docker found")
		p.Increment()
	} else {
		pterm.Warning.Println("Docker was not found on your system")
	}

	if console.CommandExists("docker-compose") == true {
		pterm.Success.Println("Docker Compose found")
		p.Increment()
	} else {
		pterm.Warning.Println("Docker Compose was not found on your system")
	}

	p.Stop()
}
